# DKX/GoogleTracer

Wrapper for Google Trace library

**Only for psr7 applications**

## Installation

```bash
$ composer require dkx/google-tracer
```

## Basic usage

```php
<?php

use DKX\GoogleTracer\Exporter\GoogleTraceExporter;
use DKX\GoogleTracer\Sampler\AlwaysSampler;
use DKX\GoogleTracer\Tracer;
use Google\Cloud\Trace\TraceClient;

$projectName = 'my-app/production';
$projectVersion = '0.0.1';

$traceClient = new TraceClient();
$exporter = new GoogleTraceExporter($traceClient);
$sampler = new AlwaysSampler();
$tracer = new Tracer($exporter, $sampler, $projectName, $projectVersion);

$trace = $tracer->start();
$request = getCurrentHttpRequestSomehow();

try {
    $response = getCurrentHttpResponseSomehow($request);
    $tracer->finishSuccessfully($trace, $request, $response);
} catch(\Throwable $e) {
    $tracer->finishWithError($trace, $request, $e);
}
```

## Create sub-spans

```php
<?php

use DKX\GoogleTracer\Span;

$trace->span('controller', function (Span $span): void {
    $users = $span->span('users.load', function (Span $span): array {
        $users = $span->span('users.load.fromCache', function (Span $span): ?array {
            return tryToLoadUsersFromCache();        
        });
        
        if ($users === null) {
            $users = $span->span('users.load.fromDatabase', function (Span $span): array {
                return loadUsersFromDatabase();
            });

            $span->span('users.load.saveCache', function (Span $span) use ($users): void {
                saveUsersToCache($users);
            });
        }

        return $users;
    });

    var_dump($users);
});
```

## PSR15 middleware

```php
<?php

use DKX\GoogleTracer\Psr15\TracerControllerSpanMiddleware;

$middleware = new TracerControllerSpanMiddleware($trace);
```

This will automatically register a `controller` span into `$trace` (can be replaced with span). Created span is added
into http request's attributes.

```php
<?php

use DKX\GoogleTracer\Psr15\TracerControllerSpanMiddleware;

/** @var \DKX\GoogleTracer\Span $span */
$span = $request->getAttribute(TracerControllerSpanMiddleware::CONTROLLER_SPAN);
$span->span('inside-of-controller', function (): void {
    // todo: watched span in a controller
});
```

Or you can use dynamic middleware which will load parent trace or span from http request attribute:

```php
<?php

use DKX\GoogleTracer\Psr15\TracerControllerSpanDynamicMiddleware;

$request = getCurrentHttpRequestSomehow();
$request->withAttribute('controllerSpanParent', $trace);

$middleware = new TracerControllerSpanDynamicMiddleware('controllerSpanParent');
``` 

## Exporters

* `DKX\GoogleTracer\Exporter\GoogleTraceExporter`: save trace data into Google Cloud Trace
* `DKX\GoogleTracer\Exporter\VoidExporter`: "do nothing" exporter

## Samplers

* `DKX\GoogleTracer\Sampler\AlwaysSampler`: save traces for all requests
* `DKX\GoogleTracer\Sampler\NeverSampler`: don't save any traces
* `DKX\GoogleTracer\Sampler\ProbabilitySampler($rate)`: save traces based on probability rate (0 <> 1)
* `DKX\GoogleTracer\Sampler\QpsSampler($cache, $cacheKey, $rate)`: save traces based on queries per second rate (0 <> 1)

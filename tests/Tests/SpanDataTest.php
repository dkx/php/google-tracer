<?php

declare(strict_types=1);

namespace DKX\GoogleTracerTests\Tests;

use DKX\GoogleTracer\SpanData;
use PHPUnit\Framework\TestCase;

final class SpanDataTest extends TestCase
{
	public function testGettersSetters(): void
	{
		$data = new SpanData('span-1', 'First span', '123');

		self::assertSame('span-1', $data->getId());
		self::assertSame('First span', $data->getName());
		self::assertSame('123', $data->getStartTime());
		self::assertNull($data->getEndTime());

		$data->setEndTime('321');

		self::assertSame('321', $data->getEndTime());
	}

	public function testGetParentSpanId(): void
	{
		$data = new SpanData('span-1', 'First span', '123', 'root-span');

		self::assertSame('root-span', $data->getParentSpanId());
	}
}

<?php

declare(strict_types=1);

namespace DKX\GoogleTracerTests\Tests;

use DKX\GoogleTracer\Factories\SpanIdFactoryInterface;
use DKX\GoogleTracer\Factories\TimestampFactoryInterface;
use DKX\GoogleTracer\Factories\TraceIdFactoryInterface;
use DKX\GoogleTracer\Span;
use DKX\GoogleTracer\Trace;
use PHPUnit\Framework\TestCase;

final class TraceTest extends TestCase
{
	protected function tearDown(): void
	{
		parent::tearDown();
		\Mockery::close();
	}

	public function testGetters(): void
	{
		$spanIdFactory = \Mockery::mock(SpanIdFactoryInterface::class);

		/** @var \DKX\GoogleTracer\Factories\TimestampFactoryInterface|\Mockery\MockInterface $timestampFactory */
		$timestampFactory = \Mockery::mock(TimestampFactoryInterface::class)
			->shouldReceive('createNow')->andReturn('123')->getMock();

		/** @var \DKX\GoogleTracer\Factories\TraceIdFactoryInterface|\Mockery\MockInterface $traceIdFactory */
		$traceIdFactory = \Mockery::mock(TraceIdFactoryInterface::class)
			->shouldReceive('create')->andReturn('trace-id')->getMock();

		$trace = new Trace($timestampFactory, $traceIdFactory, $spanIdFactory);

		self::assertSame('trace-id', $trace->getId());
		self::assertSame('123', $trace->getStart());
	}

	public function testSpan(): void
	{
		$spanIdCounter = 0;

		/** @var \DKX\GoogleTracer\Factories\SpanIdFactoryInterface|\Mockery\MockInterface $spanIdFactory */
		$spanIdFactory = \Mockery::mock(SpanIdFactoryInterface::class)
			->shouldReceive('create')->andReturnUsing(function () use (& $spanIdCounter): string {
				return 'span-'. $spanIdCounter++;
			})->getMock();

		/** @var \DKX\GoogleTracer\Factories\TimestampFactoryInterface|\Mockery\MockInterface $timestampFactory */
		$timestampFactory = \Mockery::mock(TimestampFactoryInterface::class)
			->shouldReceive('createNow')->andReturn('123')->getMock();

		/** @var \DKX\GoogleTracer\Factories\TraceIdFactoryInterface|\Mockery\MockInterface $traceIdFactory */
		$traceIdFactory = \Mockery::mock(TraceIdFactoryInterface::class)
			->shouldReceive('create')->andReturn('trace-id')->getMock();

		$trace = new Trace($timestampFactory, $traceIdFactory, $spanIdFactory);

		self::assertCount(0, $trace->getChildSpans());

		$result = $trace->span('inner-1', function (Span $span): string {
			return $span->span('inner-1-1', function (Span $span): string {
				return $span->span('inner-1-1-1', function (): string {
					return 'hello world';
				});
			});
		});

		$trace->span('inner-2', function (): void {});

		self::assertSame('hello world', $result);

		$spans = $trace->getChildSpans();
		self::assertCount(4, $spans);

		self::assertSame('inner-1', $spans[0]->getName());
		self::assertNull($spans[0]->getParentSpanId());

		self::assertSame('inner-1-1', $spans[1]->getName());
		self::assertSame($spans[0]->getId(), $spans[1]->getParentSpanId());

		self::assertSame('inner-1-1-1', $spans[2]->getName());
		self::assertSame($spans[1]->getId(), $spans[2]->getParentSpanId());

		self::assertSame('inner-2', $spans[3]->getName());
		self::assertNull($spans[3]->getParentSpanId());
	}
}

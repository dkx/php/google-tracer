<?php

declare(strict_types=1);

namespace DKX\GoogleTracerTests\Tests;

use DKX\GoogleTracer\Factories\SpanIdFactory;
use DKX\GoogleTracer\Factories\TimestampFactory;
use DKX\GoogleTracer\Span;
use DKX\GoogleTracer\SpanStorage;
use PHPUnit\Framework\TestCase;

final class SpanTest extends TestCase
{
	/** @var \DKX\GoogleTracer\SpanStorage */
	private $storage;

	protected function tearDown(): void
	{
		parent::tearDown();
		\Mockery::close();
	}

	public function testGetId(): void
	{
		$span = $this->createSpan();
		self::assertIsString($span->getId());
	}

	public function testRun(): void
	{
		$span = $this->createSpan();

		self::assertCount(0, $this->storage->getSpans());

		$result = $span->run(function (): string {
			return 'hello world';
		});

		self::assertSame('hello world', $result);

		$spans = $this->storage->getSpans();
		self::assertCount(1, $spans);
		self::assertIsString($spans[0]->getId());
		self::assertSame('span', $spans[0]->getName());
		self::assertIsString($spans[0]->getStartTime());
		self::assertIsString($spans[0]->getEndTime());
		self::assertNull($spans[0]->getParentSpanId());
	}

	public function testSpan(): void
	{
		$span = $this->createSpan();

		self::assertCount(0, $this->storage->getSpans());

		$result = $span->span('inner-1', function (Span $span): string {
			return $span->span('inner-1-1', function (Span $span): string {
				return $span->span('inner-1-1-1', function (): string {
					return 'hello world';
				});
			});
		});

		$span->span('inner-2', function (): void {});

		self::assertSame('hello world', $result);

		$spans = $this->storage->getSpans();
		self::assertCount(4, $spans);

		self::assertSame('inner-1', $spans[0]->getName());
		self::assertSame($span->getId(), $spans[0]->getParentSpanId());

		self::assertSame('inner-1-1', $spans[1]->getName());
		self::assertSame($spans[0]->getId(), $spans[1]->getParentSpanId());

		self::assertSame('inner-1-1-1', $spans[2]->getName());
		self::assertSame($spans[1]->getId(), $spans[2]->getParentSpanId());

		self::assertSame('inner-2', $spans[3]->getName());
		self::assertSame($span->getId(), $spans[3]->getParentSpanId());
	}

	private function createSpan(): Span
	{
		return new Span(
			$this->storage = new SpanStorage(),
			new TimestampFactory(),
			new SpanIdFactory(),
			'span'
		);
	}
}

<?php

declare(strict_types=1);

namespace DKX\GoogleTracerTests\Tests;

use DKX\GoogleTracer\SpanData;
use DKX\GoogleTracer\SpanStorage;
use PHPUnit\Framework\TestCase;

final class SpanStorageTest extends TestCase
{
	public function testStorage(): void
	{
		$storage = new SpanStorage();

		self::assertCount(0, $storage->getSpans());

		$data = new SpanData('span-1', 'First span', '123');
		$storage->addSpan($data);

		self::assertEquals([$data], $storage->getSpans());
	}
}

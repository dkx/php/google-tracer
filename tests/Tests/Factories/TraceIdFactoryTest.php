<?php

declare(strict_types=1);

namespace DKX\GoogleTracerTests\Tests\Factories;

use DKX\GoogleTracer\Factories\TraceIdFactory;
use PHPUnit\Framework\TestCase;

final class TraceIdFactoryTest extends TestCase
{
	public function testCreate(): void
	{
		$factory = new TraceIdFactory();
		self::assertIsString($factory->create());
	}
}

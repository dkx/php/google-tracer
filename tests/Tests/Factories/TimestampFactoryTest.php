<?php

declare(strict_types=1);

namespace DKX\GoogleTracerTests\Tests\Factories;

use DKX\GoogleTracer\Factories\TimestampFactory;
use PHPUnit\Framework\TestCase;

final class TimestampFactoryTest extends TestCase
{
	public function testCreate(): void
	{
		$factory = new TimestampFactory();
		self::assertIsString($factory->createNow());
	}
}

<?php

declare(strict_types=1);

namespace DKX\GoogleTracerTests\Tests\Factories;

use DKX\GoogleTracer\Factories\SpanIdFactory;
use PHPUnit\Framework\TestCase;

final class SpanIdFactoryTest extends TestCase
{
	public function testCreate(): void
	{
		$factory = new SpanIdFactory();
		self::assertIsString($factory->create());
	}
}

<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Psr15;

use DKX\GoogleTracer\SpannableInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class TracerControllerSpanMiddleware extends BaseControllerSpanMiddleware implements MiddlewareInterface
{
	/** @var SpannableInterface */
	private $spannable;

	public function __construct(SpannableInterface $spannable, string $spanName = self::SPAN_NAME)
	{
		parent::__construct($spanName);

		$this->spannable = $spannable;
	}

	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		return $this->processRequest($this->spannable, $request, $handler);
	}

}

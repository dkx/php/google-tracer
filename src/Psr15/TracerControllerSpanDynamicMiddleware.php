<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Psr15;

use DKX\GoogleTracer\SpannableInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class TracerControllerSpanDynamicMiddleware extends BaseControllerSpanMiddleware implements MiddlewareInterface
{
	public const REQUEST_SPAN_ATTRIBUTE_NAME = 'tracer.trace';

	/** @var string */
	private $requestSpanAttributeName;

	public function __construct(string $requestSpanAttributeName = self::REQUEST_SPAN_ATTRIBUTE_NAME, string $spanName = self::SPAN_NAME)
	{
		parent::__construct($spanName);

		$this->requestSpanAttributeName = $requestSpanAttributeName;
	}

	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		/** @var SpannableInterface|null $spannable */
		$spannable = $request->getAttribute($this->requestSpanAttributeName);
		if ($spannable === null) {
			return $handler->handle($request);
		}

		return $this->processRequest($spannable, $request, $handler);
	}

}

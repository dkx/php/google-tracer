<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Psr15;

use DKX\GoogleTracer\Span;
use DKX\GoogleTracer\SpannableInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

abstract class BaseControllerSpanMiddleware
{
	public const CONTROLLER_SPAN = 'tracer.span.controller';

	public const SPAN_NAME = 'controller';

	/** @var string */
	private $spanName;

	public function __construct(string $spanName)
	{
		$this->spanName = $spanName;
	}

	protected function processRequest(SpannableInterface $spannable, ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
	{
		return $spannable->span($this->spanName, function (Span $span) use ($request, $handler): ResponseInterface {
			$request = $request->withAttribute(self::CONTROLLER_SPAN, $span);
			return $handler->handle($request);
		});
	}
}

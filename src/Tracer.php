<?php

declare(strict_types=1);

namespace DKX\GoogleTracer;

use DKX\GoogleTracer\Exporter\Exporter;
use DKX\GoogleTracer\Factories\SpanIdFactory;
use DKX\GoogleTracer\Factories\TimestampFactory;
use DKX\GoogleTracer\Factories\TraceIdFactory;
use DKX\GoogleTracer\Sampler\Sampler;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class Tracer
{
	/** @var Exporter */
	private $exporter;

	/** @var Sampler */
	private $sampler;

	/** @var TimestampFactory */
	private $timestampFactory;

	/** @var TraceIdFactory */
	private $traceIdFactory;

	/** @var SpanIdFactory */
	private $spanIdFactory;

	/** @var string */
	private $projectName;

	/** @var string|null */
	private $projectVersion;

	/** @var string[] */
	private $ignoredPaths = [];

	public function __construct(Exporter $exporter, Sampler $sampler, string $projectName, ?string $projectVersion = null)
	{
		$this->exporter = $exporter;
		$this->sampler = $sampler;
		$this->projectName = $projectName;
		$this->projectVersion = $projectVersion;
		$this->timestampFactory = new TimestampFactory();
		$this->traceIdFactory = new TraceIdFactory();
		$this->spanIdFactory = new SpanIdFactory();
	}

	public function start(): Trace
	{
		return new Trace($this->timestampFactory, $this->traceIdFactory, $this->spanIdFactory);
	}

	/**
	 * @param string[] $paths
	 */
	public function setIgnoredPaths(array $paths): void
	{
		$this->ignoredPaths = $paths;
	}

	public function addIgnoredPath(string $path): void
	{
		$this->ignoredPaths[] = $path;
	}

	public function finishSuccessfully(Trace $trace, ServerRequestInterface $request, ResponseInterface $response): void
	{
		if ($this->shouldIgnore($request) || !$this->sampler->shouldSample()) {
			return;
		}

		$this->exporter->saveSuccessRequest($trace, $request, $response, $this->projectName, $this->projectVersion);
	}

	public function finishWithError(Trace $trace, ServerRequestInterface $request, \Throwable $e): void
	{
		if ($this->shouldIgnore($request) || !$this->sampler->shouldSample()) {
			return;
		}

		$this->exporter->saveErrorRequest($trace, $request, $e, $this->projectName, $this->projectVersion);
	}

	private function shouldIgnore(ServerRequestInterface $request): bool
	{
		return \in_array($request->getUri()->getPath(), $this->ignoredPaths, true);
	}
}

<?php

declare(strict_types=1);

namespace DKX\GoogleTracer;

interface SpanStorageInterface
{
	/**
	 * @return SpanData[]
	 */
	public function getSpans(): array;

	public function addSpan(SpanData $span): void;
}

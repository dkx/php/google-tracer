<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Factories;

use Google\Cloud\Trace\TimestampTrait;

final class TimestampFactory implements TimestampFactoryInterface
{
	use TimestampTrait;

	public function createNow(): string
	{
		return $this->formatDate();
	}
}

<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Factories;

use Ramsey\Uuid\Uuid;

final class TraceIdFactory implements TraceIdFactoryInterface
{
	public function create(): string
	{
		return str_replace('-', '', Uuid::uuid4()->toString());
	}
}

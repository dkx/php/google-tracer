<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Factories;

interface TraceIdFactoryInterface
{
	public function create(): string;
}

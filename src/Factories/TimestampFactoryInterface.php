<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Factories;

interface TimestampFactoryInterface
{
	public function createNow(): string;
}

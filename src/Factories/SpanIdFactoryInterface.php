<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Factories;

interface SpanIdFactoryInterface
{
	public function create(): string;
}

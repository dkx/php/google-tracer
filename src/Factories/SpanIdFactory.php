<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Factories;

final class SpanIdFactory implements SpanIdFactoryInterface
{
	public function create(): string
	{
		return dechex(mt_rand());
	}
}

<?php

declare(strict_types=1);

namespace DKX\GoogleTracer;

final class SpanStorage implements SpanStorageInterface
{
	/** @var SpanData[] */
	private $spans = [];

	/**
	 * @return SpanData[]
	 */
	public function getSpans(): array
	{
		return $this->spans;
	}

	public function addSpan(SpanData $data): void
	{
		$this->spans[] = $data;
	}
}

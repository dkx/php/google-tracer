<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Sampler;

interface Sampler
{
	public function shouldSample(): bool;
}

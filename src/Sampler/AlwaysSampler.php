<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Sampler;

final class AlwaysSampler implements Sampler
{
	public function shouldSample(): bool
	{
		return true;
	}
}

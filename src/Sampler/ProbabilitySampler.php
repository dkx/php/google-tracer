<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Sampler;

final class ProbabilitySampler implements Sampler
{
	/** @var float */
	private $rate;

	public function __construct(float $rate)
	{
		if ($rate > 1 || $rate < 0) {
			throw new \InvalidArgumentException('Percentage must be between 0 and 1');
		}

		$this->rate = $rate;
	}

	public function shouldSample(): bool
	{
		return \lcg_value() <= $this->rate;
	}
}

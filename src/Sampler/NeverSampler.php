<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Sampler;

final class NeverSampler implements Sampler
{
	public function shouldSample(): bool
	{
		return false;
	}
}

<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Sampler;

use Psr\SimpleCache\CacheInterface;

final class QpsSampler implements Sampler
{
	/** @var CacheInterface */
	private $cache;

	/** @var string */
	private $key;

	/** @var float */
	private $rate;

	public function __construct(CacheInterface $cache, string $key, float $rate)
	{
		if ($rate > 1 || $rate < 0) {
			throw new \InvalidArgumentException('QPS sampling rate must be less that 1 query per second');
		}

		$this->cache = $cache;
		$this->key = $key;
		$this->rate = $rate;
	}

	public function shouldSample(): bool
	{
		$now = \microtime(true);

		if ($this->cache->has($this->key)) {
			$item = $this->cache->get($this->key);
			if ((float) $item > $now) {
				return false;
			}
		}

		$this->cache->set($this->key, $now + 1.0 / $this->rate);

		return true;
	}
}

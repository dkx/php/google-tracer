<?php

declare(strict_types=1);

namespace DKX\GoogleTracer;

use DKX\GoogleTracer\Factories\SpanIdFactoryInterface;
use DKX\GoogleTracer\Factories\TimestampFactoryInterface;

final class Span implements SpannableInterface
{
	/** @var SpanStorageInterface */
	private $spanStorage;

	/** @var TimestampFactoryInterface */
	private $timestampFactory;

	/** @var SpanIdFactoryInterface */
	private $spanIdFactory;

	/** @var string */
	private $name;

	/** @var Span|null */
	private $parent;

	/** @var string */
	private $id;

	public function __construct(
		SpanStorageInterface $spanStorage,
		TimestampFactoryInterface $timestampFactory,
		SpanIdFactoryInterface $spanIdFactory,
		string $name,
		?Span $parent = null
	) {
		$this->spanStorage = $spanStorage;
		$this->timestampFactory = $timestampFactory;
		$this->spanIdFactory = $spanIdFactory;
		$this->name = $name;
		$this->parent = $parent;
		$this->id = $this->spanIdFactory->create();
	}

	public function getId(): string
	{
		return $this->id;
	}

	/**
	 * @param callable $fn
	 * @return mixed
	 */
	public function run(callable $fn)
	{
		$start = $this->timestampFactory->createNow();
		$runningSpan = new SpanData(
			$this->id,
			$this->name,
			$start,
			$this->parent === null ? null : $this->parent->getId()
		);

		$this->spanStorage->addSpan($runningSpan);

		$result = \call_user_func($fn, $this);
		$end = $this->timestampFactory->createNow();
		$runningSpan->setEndTime($end);

		return $result;
	}

	/**
	 * @param string $name
	 * @param callable $fn
	 * @return mixed
	 */
	public function span(string $name, callable $fn)
	{
		$inner = new self($this->spanStorage, $this->timestampFactory, $this->spanIdFactory, $name, $this);
		return $inner->run($fn);
	}
}

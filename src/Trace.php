<?php

declare(strict_types=1);

namespace DKX\GoogleTracer;

use DKX\GoogleTracer\Factories\SpanIdFactoryInterface;
use DKX\GoogleTracer\Factories\TimestampFactoryInterface;
use DKX\GoogleTracer\Factories\TraceIdFactoryInterface;

final class Trace implements SpannableInterface
{
	/** @var TimestampFactoryInterface */
	private $timestampFactory;

	/** @var SpanIdFactoryInterface */
	private $spanIdFactory;

	/** @var SpanStorage */
	private $spanStorage;

	/** @var string */
	private $id;

	/** @var string */
	private $start;

	public function __construct(
		TimestampFactoryInterface $timestampFactory,
		TraceIdFactoryInterface $traceIdFactory,
		SpanIdFactoryInterface $spanIdFactory
	) {
		$this->timestampFactory = $timestampFactory;
		$this->spanIdFactory = $spanIdFactory;

		$this->spanStorage = new SpanStorage();
		$this->id = $traceIdFactory->create();
		$this->start = $this->timestampFactory->createNow();
	}

	public function getId(): string
	{
		return $this->id;
	}

	public function getStart(): string
	{
		return $this->start;
	}

	/**
	 * @param string $name
	 * @param callable $fn
	 * @return mixed
	 */
	public function span(string $name, callable $fn)
	{
		$span = new Span($this->spanStorage, $this->timestampFactory, $this->spanIdFactory, $name);
		return $span->run($fn);
	}

	/**
	 * @return SpanData[]
	 */
	public function getChildSpans(): array
	{
		return $this->spanStorage->getSpans();
	}

	public function getLogTracePath(string $projectId): string
	{
		return 'projects/' . $projectId . '/traces/' . $this->id;
	}
}

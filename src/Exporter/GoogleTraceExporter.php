<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Exporter;

use DKX\GoogleTracer\Factories\TimestampFactory;
use DKX\GoogleTracer\SpanData;
use DKX\GoogleTracer\Trace;
use Google\Cloud\Trace as GoogleTrace;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class GoogleTraceExporter implements Exporter
{
	/** @var GoogleTrace\TraceClient */
	private $client;

	/** @var TimestampFactory */
	private $timestampFactory;

	public function __construct(GoogleTrace\TraceClient $client)
	{
		$this->client = $client;
		$this->timestampFactory = new TimestampFactory();
	}

	public function saveSuccessRequest(Trace $trace, ServerRequestInterface $request, ResponseInterface $response, string $projectName, ?string $projectVersion): void
	{
		$this->save($trace, $request, [
			'/http/status_code' => $response->getStatusCode(),
			'/http/response/size' => $response->getBody()->getSize(),
		], $projectName, $projectVersion);
	}

	public function saveErrorRequest(Trace $trace, ServerRequestInterface $request, \Throwable $e, string $projectName, ?string $projectVersion): void
	{
		$this->save($trace, $request, [
			'/http/status_code' => $this->determineStatusCode($request),
			'/error/name' => \get_class($e),
			'/error/message' => $e->getMessage(),
		], $projectName, $projectVersion);
	}

	/**
	 * @param mixed[] $attributes
	 */
	private function save(Trace $trace, ServerRequestInterface $request, array $attributes, string $projectName, ?string $projectVersion): void
	{
		$rootSpan = $this->createAndFinishRootSpan($trace, $request, $attributes, $projectName, $projectVersion);
		$childSpans = $this->getTraceChildSpans($trace);

		$googleTrace = $this->client->trace($trace->getId());
		$googleTrace->setSpans(\array_merge([$rootSpan], $childSpans));

		$this->client->insert($googleTrace);
	}

	/**
	 * @param mixed[] $attributes
	 */
	private function createAndFinishRootSpan(Trace $trace, ServerRequestInterface $request, array $attributes, string $projectName, ?string $projectVersion = null): GoogleTrace\Span
	{
		$uri = $request->getUri();

		$span = [
			'name' => (string) $request->getUri(),
			'startTime' => $trace->getStart(),
			'endTime' => $this->timestampFactory->createNow(),
			'attributes' => [
				'g.co/gae/app/module' => $projectName,
				'/http/method' => $request->getMethod(),
				'/http/host' => $uri->getHost(),
				'/http/path' => $uri->getPath(),
				'/http/url' => (string) $uri,
			],
		];

		if ($projectVersion !== null) {
			$span['attributes']['g.co/gae/app/version'] = $projectVersion;
		}

		$agent = $request->getHeaderLine('user-agent');
		if ($agent !== '') {
			$span['attributes']['/http/user_agent'] = $agent;
		}

		$requestSize = $request->getBody()->getSize();
		if ($requestSize !== null) {
			$span['attributes']['/http/request/size'] = $requestSize;
		}

		$span['attributes'] = \array_merge($span['attributes'], $attributes);

		return new GoogleTrace\Span($trace->getId(), $span);
	}

	/**
	 * @return GoogleTrace\Span[]
	 */
	private function getTraceChildSpans(Trace $trace): array
	{
		return \array_map(function (SpanData $data) use ($trace): GoogleTrace\Span {
			$spanData = [
				'spanId' => $data->getId(),
				'name' => $data->getName(),
				'startTime' => $data->getStartTime(),
			];

			$endTime = $data->getEndTime();
			if ($endTime !== null) {
				$spanData['endTime'] = $endTime;
			}

			$parentSpanId = $data->getParentSpanId();
			if ($parentSpanId !== null) {
				$spanData['parentSpanId'] = $parentSpanId;
			}

			return new GoogleTrace\Span($trace->getId(), $spanData);
		}, $trace->getChildSpans());
	}

	protected function determineStatusCode(ServerRequestInterface $request): int
	{
		if ($request->getMethod() === 'OPTIONS') {
			return 200;
		}

		return 500;
	}
}

<?php

declare(strict_types=1);

namespace DKX\GoogleTracer\Exporter;

use DKX\GoogleTracer\Trace;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class VoidExporter implements Exporter
{
	public function saveSuccessRequest(Trace $trace, ServerRequestInterface $request, ResponseInterface $response, string $projectName, ?string $projectVersion): void
	{
	}

	public function saveErrorRequest(Trace $trace, ServerRequestInterface $request, \Throwable $e, string $projectName, ?string $projectVersion): void
	{
	}
}

<?php

declare(strict_types=1);

namespace DKX\GoogleTracer;

interface SpannableInterface
{
	/**
	 * @param string $name
	 * @param callable $fn
	 * @return mixed
	 */
	public function span(string $name, callable $fn);
}

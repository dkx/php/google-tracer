<?php

declare(strict_types=1);

namespace DKX\GoogleTracer;

final class SpanData
{
	/** @var string */
	private $id;

	/** @var string */
	private $name;

	/** @var string */
	private $startTime;

	/** @var string|null */
	private $endTime;

	/** @var string|null */
	private $parentSpanId;

	public function __construct(string $id, string $name, string $startTime, ?string $parentSpanId = null)
	{
		$this->id = $id;
		$this->name = $name;
		$this->startTime = $startTime;
		$this->parentSpanId = $parentSpanId;
	}

	public function getId(): string
	{
		return $this->id;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getStartTime(): string
	{
		return $this->startTime;
	}

	public function getEndTime(): ?string
	{
		return $this->endTime;
	}

	public function setEndTime(string $endTime): void
	{
		$this->endTime = $endTime;
	}

	public function getParentSpanId(): ?string
	{
		return $this->parentSpanId;
	}
}
